package queue;


import java.util.*;

class Main {
	public static void main(String args[]) {
		TeamQueueList list = new TeamQueueList();
		System.out.println(list);
		for (int i = 0; i < 10; i++) {
			list.add(i + 1);
		}
		System.out.println(list);
		list.popLeft();
		System.out.println(list);
		list.add(17);
		System.out.println(list);
		while (!list.isEmpty()) {
			list.popLeft();
			System.out.println(list);
		}
		long init = System.currentTimeMillis();
		for (int i = 0; i < 200_000; i++) {
			list.add(i);
		}
		long end = System.currentTimeMillis();
		System.out.printf("After inserting %d values %.2f secs%n", list.size(), (end - init) / 1000.);
		Scanner sc = new Scanner(System.in);
		System.out.printf("Do you really want to print the list?");
		if (sc.next().equalsIgnoreCase("yes")) {
			System.out.println(list);
		} else {
			System.out.println("Uff!");
		}
	}
}

class TeamQueueList {

	// fields
	private TeamQueueNode head;
	private TeamQueueNode tail;
	private int length;
	// related fields to the problem Team Queue
	private int[] groupPerMember;
	private int numberOfGroups;
	private TeamQueueNode[] lastMemberPerGroup;

	// constructors
	public TeamQueueList() {
		this.head = null;
		this.tail = null;
		this.length = 0;
	}

	/**
	 * This method may be used to do enqueue operation
	 */
	public void enqueue(int value) {

		TeamQueueNode node = new TeamQueueNode(value);

		if (emptyHead()) {
			this.head = node;
			this.tail = node;
		} else {
			this.tail.setNext(node);
			TeamQueueNode cristo = this.tail.getNext();
			this.tail = cristo;
		}
	}

	public TeamQueueNode getTail() {
		return this.tail;
	}

	public TeamQueueNode getHead() {
		return this.head;
	}

	public boolean emptyHead() {
		return this.head == null ? true : false;
	}

	/**
	 * This method may be used to do dequeue operation
	 */
	public void dequeue() {

		if (this.head != null) {

			TeamQueueNode nodoSalida = this.head;

			TeamQueueNode cristo = this.head.getNext();

			this.head = cristo;
		}

	}

	public void add(int value) {
		TeamQueueNode lastNode = this.getLastNode();
		TeamQueueNode newNode = new TeamQueueNode(value);
		if (lastNode == null) { // list is empty
			this.head = newNode;
		} else {
			lastNode.next = newNode;
		}
		this.tail = newNode;
		this.length += 1;
	}

	public int popLeft() {

		if (this.isEmpty())
			throw new NoSuchElementException();
		TeamQueueNode currentNode = this.head;
		this.head = this.head.next;

		if (this.length == 1)
			this.tail = null;
		this.length -= 1;
		return currentNode.value;
	}

	public int size() {
		return this.length;
	}

	public TeamQueueNode getLastNode() {
		if (this.isEmpty()) {
			return null;
		} else {
			return this.tail;
		}
	}

	public boolean isEmpty() {
		return this.tail == null;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (TeamQueueNode currentNode = this.head; currentNode != null;) {
			sb.append(String.valueOf(currentNode.value));
			sb.append(", ");
			currentNode = currentNode.next;
		}
		if (sb.length() > 1) {
			sb.replace(sb.length() - 2, sb.length(), "");
		}
		sb.append("]");
		return sb.toString();
	}

}

class TeamQueueNode {
	// fields
	protected int value;
	protected TeamQueueNode next;

	public TeamQueueNode(int value) {
		this.value = value;
		this.next = null;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public TeamQueueNode getNext() {
		return next;
	}

	public void setNext(TeamQueueNode next) {
		this.next = next;
	}

}

