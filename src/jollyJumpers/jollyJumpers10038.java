package jollyJumpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class jollyJumpers10038 {
	
	public static void main(String[] args) throws IOException {
		
		
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		
		String line = bf.readLine();
		
		
		while(line.length() > 1) {
			
			StringTokenizer st = new StringTokenizer(line);
			
			int n = Integer.parseInt(st.nextToken());
			
			int [] valoresAbsolutos = new int[n-1];
			
			int [] valores = new int[n];
			
			//lectura
			for (int i = 0; i < n; i++) {
				
				valores[i] = Integer.parseInt(st.nextToken());
				
			}
			
			//calculo de valores
			
			for (int i = 0; i < valores.length - 1; i++) {
				
				valoresAbsolutos[i] = Math.abs((valores[i] - valores[i+1]));
				
			}
			
			Arrays.sort(valoresAbsolutos);
			
			//validacion Jolly
			
			boolean jolly = true;
			
			for (int i = 0; i < valoresAbsolutos.length ; i++) {
				
				if(valoresAbsolutos[i]!= (i+1)) {
					jolly = false;
					break;
				}
				
			}
			
			if(jolly) {
				System.out.println("Jolly");
			}else {
				System.out.println("Not jolly");
			}
			
			line = bf.readLine();
			
			
		}
		
		
	}
	

}