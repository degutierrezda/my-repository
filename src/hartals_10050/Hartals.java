package hartals_10050;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Hartals {
	
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		String line = bf.readLine();
		
		int casos = Integer.parseInt(line);
		
		for (int i = 0; i < casos; i++) {
			
			line = bf.readLine();
			
			int diasSimulacion = Integer.parseInt(line);
			
			boolean simulacion [] = new boolean [diasSimulacion + 1];
			
			//falso toca trabajar 
			
			Arrays.fill(simulacion, false);
			
			int numPartidos = Integer.parseInt(bf.readLine());
			
			while(numPartidos!=0) {
				
				int hi = Integer.parseInt(bf.readLine());
				
				for (int j = hi; j < simulacion.length; j = j + hi) {
					
					
					simulacion[j] = true;
					
					
				}
				
				
				numPartidos--;
			}
			
			for (int j = 6; j < simulacion.length; j=j+7) {
				simulacion[j]=false;
			}
			
			for (int j = 7; j < simulacion.length; j=j+7) {
				simulacion[j]=false;
			}
			
			
			int diasNoTrabajo = 0;
			
			for (int j = 0; j < simulacion.length; j++) {
				
				if(simulacion[j]) {
					diasNoTrabajo++;
				}
				
			}
			
			
			
			System.out.println(diasNoTrabajo);
		}
		
				
	}

	
	
}