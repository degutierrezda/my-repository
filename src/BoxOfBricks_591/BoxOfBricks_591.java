package BoxOfBricks_591;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BoxOfBricks_591 {
	
	
	public static void main(String[] args) throws IOException {
		
		
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		String line = bf.readLine();
		
		int set = 0;
		
		while(!line.equals("0")) {
			
			set++;
			
			int towers = Integer.parseInt(line);
			
			int [] towersArray = new int[towers];
			
			line = bf.readLine();
			
			StringTokenizer st = new StringTokenizer(line);
			
			int z = 0 ;
			
			for (int i = 0; i < towersArray.length; i++) {
				towersArray[i] = Integer.parseInt(st.nextToken());
				z = z + towersArray[i];
			}
			int promLenght = z / towers;
			
			int movs = 0;
			
			for (int i = 0; i < towersArray.length; i++) {
				
				if(towersArray[i]>promLenght) {
					movs = movs + (towersArray[i]-promLenght);
				}
			}
			
			System.out.println("Set #"+set);
			System.out.println("The minimum number of moves is "+movs+".");
			System.out.println();
			
			
			
			
			line = bf.readLine();
		}
		
		
	}
	

}